import { RootState } from '../init/store';

export const getErrorMessage = (state: RootState) => {
    return state.ui.errorMessage;
};

export const getSuccessMessage = (state: RootState) => {
    return state.ui.successMessage;
};

