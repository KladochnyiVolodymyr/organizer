import { useSelector } from 'react-redux';
import { getTodoList } from '../lib/redux/selectors/todoList';
import { ITodoModel } from '../types';
import { ITaskModel } from '../types/TaskModel';

export const useCurrentTask = ():ITodoModel => {
    const { selectedTodoId, todos } = useSelector(getTodoList);

    const currentTaskIndex = todos.findIndex((task: ITaskModel) => task.id === selectedTodoId);

    return todos[ currentTaskIndex ];
};
