// Core
import * as yup from 'yup';
import { ITaskFormShape } from '../types';

// eslint-disable-next-line no-template-curly-in-string
const tooShortMessage = 'мінімальна довжина - ${min} символів';
// eslint-disable-next-line no-template-curly-in-string
const requiredMessage = '*';

export const schema: yup.SchemaOf<ITaskFormShape> = yup.object().shape({
    title: yup
        .string()
        .min(3, tooShortMessage)
        .required(requiredMessage),
    description: yup
        .string()
        .min(3, tooShortMessage)
        .required(requiredMessage),
    deadline: yup
        .date()
        .required(requiredMessage),
    tag: yup
        .string()
        .required(requiredMessage),
    completed: yup
        .boolean()
        .required(requiredMessage),
});
