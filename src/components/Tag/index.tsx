import { ITagModel } from '../../types';

interface IProps extends ITagModel {
    isSelected: boolean,
    onSelect: (id: string) => void
}

export const Tag = (props: IProps) => {
    const {
        id, bg, color, name, isSelected, onSelect,
    } = props;

    return (
        <span
            onClick = { () => onSelect(id) }
            key = { id }
            className = { isSelected ? 'tag selected' : 'tag' }
            style = { { backgroundColor: bg, color } }>{ name }</span>
    );
};
