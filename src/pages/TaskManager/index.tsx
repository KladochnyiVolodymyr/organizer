import { useSelector } from 'react-redux';
import { useEffect } from 'react';
import { Controls } from '../../components/Controls';
import { List } from '../../components/List';
import { TaskCard } from '../../components/forms/TaskCard';
import { useAppDispatch } from '../../lib/redux/init/store';
import { newTaskActions } from '../../lib/redux/actions';
import { getNewTaskState } from '../../lib/redux/selectors';


export const TaskManager: React.FC = () => {
    const dispatch = useAppDispatch();
    const { isOpenNewTask } = useSelector(getNewTaskState);

    useEffect(() => {
        dispatch(newTaskActions.fetchTagsAsync());
    }, []);


    return (
        <>
            <Controls />
            <div className = 'wrap'>
                <List />
                { isOpenNewTask && (
                    <TaskCard />
                ) }

            </div>
        </>
    );
};
