import { RootState } from '../init/store';
import { initialStateTodos } from '../reducers/todoList';

export const getTodoList = (state: RootState): typeof initialStateTodos => {
    return state.todoList;
};

