export const todoListTypes = Object.freeze({
    INIT_TODO_LIST:       'INIT_TODO_LIST',
    ADD_TODO:             'ADD_TODO',
    UPDATE_TODO:          'UPDATE_TODO',
    DELETE_TODO:          'DELETE_TODO',
    SET_SELECTED_TODO_ID: 'SET_SELECTED_TODO_ID',
});
