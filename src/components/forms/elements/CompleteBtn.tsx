interface IProps {
    value: boolean,
    onChange: (status: boolean) => void
}
export const CompleteBtn = (props: IProps) => {
    const { onChange } = props;

    return (
        <button onClick = { () => onChange(true) } className = 'button-complete-task'>
            Виконати
        </button>
    );
};
