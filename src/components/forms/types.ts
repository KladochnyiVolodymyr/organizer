export interface ILoginFormShape {
    email: string;
    password: string;
}

export interface ISignUpFormShape {
    name: string;
    email: string;
    password: string;
    confirmPassword: string;
}

export interface ITaskFormShape {
    completed: boolean;
    title: string;
    description: string;
    deadline: Date,
    tag: string;
}
