import { AnyAction } from 'redux';
import { ITodoModel } from '../../../types';
import { todoListTypes } from '../types';

export const initialStateTodos = {
    todos:          [],
    selectedTodoId: '',
};

// eslint-disable-next-line default-param-last
export const todoListReducer = (state = initialStateTodos, action:AnyAction) => {
    switch (action.type) {
        case todoListTypes.INIT_TODO_LIST: {
            return {
                ...state,
                todos: action.payload.data,
            };
        }

        case todoListTypes.ADD_TODO: {
            return {
                ...state,
                todos: [action.payload.data, ...state.todos],
            };
        }

        case todoListTypes.UPDATE_TODO: {
            return {
                ...state,
                todos: state.todos.map((item:ITodoModel) => {
                    if (item.id === action.payload.data.id) {
                        return { ...item, ...action.payload.data };
                    }

                    return item;
                }),
            };
        }

        case todoListTypes.DELETE_TODO: {
            return {
                ...state,
                todos: state.todos.filter((item:ITodoModel) => item.id !== action.payload),
            };
        }

        case todoListTypes.SET_SELECTED_TODO_ID: {
            return {
                ...state,
                selectedTodoId: action.payload,
            };
        }

        default: {
            return state;
        }
    }
};
