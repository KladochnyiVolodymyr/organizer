import { api } from '../../../api';
import { ITodoModel } from '../../../types';
import { ITaskModel } from '../../../types/TaskModel';
import { AppThunk } from '../init/store';
import { todoListTypes } from '../types';
import { uiActions } from './ui';


export const todoList = Object.freeze({

    initTodos: (todos: ITodoModel[]) => {
        return {
            type:    todoListTypes.INIT_TODO_LIST,
            payload: todos,
        };
    },

    addTodo: (todo: ITaskModel) => {
        return {
            type:    todoListTypes.ADD_TODO,
            payload: todo,
        };
    },

    updateTodo: (todo: ITaskModel) => {
        return {
            type:    todoListTypes.UPDATE_TODO,
            payload: todo,
        };
    },

    deleteTodo: (id: string) => {
        return {
            type:    todoListTypes.DELETE_TODO,
            payload: id,
        };
    },

    setSelectedTodoId: (selectedTodoId: string) => {
        return {
            type:    todoListTypes.SET_SELECTED_TODO_ID,
            payload: selectedTodoId,
        };
    },

    fetchTodoListAsync: (): AppThunk => async (dispatch) => {
        try {
            const todos = await api.todos.fetch();

            dispatch(todoList.initTodos(todos));
        } catch (error:any) {
            dispatch(uiActions.setError(error.message));
        }
    },
});
