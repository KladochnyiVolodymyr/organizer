// Core
import * as yup from 'yup';
import { ISignUpFormShape } from '../types';

// eslint-disable-next-line no-template-curly-in-string
const tooShortMessage = 'мінімальна довжина - ${min} символів';
// eslint-disable-next-line no-template-curly-in-string
const tooLongMessage = 'максимальна довжина - ${max} символів';
// eslint-disable-next-line no-template-curly-in-string
const requiredMessage = 'поле обов\'язкове до заповнення';

export const schema: yup.SchemaOf<ISignUpFormShape> = yup.object().shape({
    name: yup
        .string()
        .min(5, tooShortMessage)
        .max(40, tooLongMessage)
        .required(requiredMessage),
    email: yup
        .string()
        .email()
        .required(requiredMessage),
    password: yup
        .string()
        .min(8, tooShortMessage)
        .max(16, tooLongMessage)
        .required(requiredMessage),
    confirmPassword: yup
        .string()
        .oneOf([yup.ref('password')], 'Паролі повинні співпадати')
        .required(requiredMessage),
});

export interface ISignUp extends Omit<ISignUpFormShape, 'confirmPassword'>{}
