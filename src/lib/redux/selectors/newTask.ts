import { RootState } from '../init/store';
import { initialState } from '../reducers/newTask';

export const getNewTaskState = (state: RootState): typeof initialState => {
    return state.newTask;
};

