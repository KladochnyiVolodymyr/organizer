// Core
import { FC } from 'react';
import {
    Navigate, Outlet, Route, Routes,
} from 'react-router-dom';
import { ToastContainer, Slide } from 'react-toastify';


// Components
import { Footer } from './components/Footer';
import { Navigation } from './components/Navigation';


// Pages
import { SignUp } from './pages/SignUp';
import { Login } from './pages/Login';
import { TaskManager } from './pages/TaskManager';
import { Profile } from './pages/Profile';


// Hooks
import { useErrorMessage } from './hooks/useUiMessage';


export const App: FC = () => {
    useErrorMessage();

    return (
        <>
            <ToastContainer newestOnTop transition = { Slide } />
            <Navigation />
            <main>
                <Routes>
                    <Route path = '/task-manager' element = { <Outlet /> }>
                        <Route path = '/' element = { <TaskManager /> } />
                    </Route>

                    <Route path = '/profile' element = { <Profile /> } />
                    <Route path = '/login' element = { <Login /> } />
                    <Route path = '/signup' element = { <SignUp /> } />

                    <Route path = '*' element = { <Navigate to = '/task-manager' replace /> } />
                </Routes>
            </main>
            <Footer />
        </>
    );
};

