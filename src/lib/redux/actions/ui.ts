import { uiTypes } from '../types';

export const uiActions = Object.freeze({
    resetError: () => {
        return {
            type: uiTypes.RESET_ERROR,
        };
    },

    setError: (message: string) => {
        return {
            type:    uiTypes.SET_ERROR,
            error:   true,
            payload: message,
        };
    },

    successMessage: (message: string) => {
        return {
            type:    uiTypes.SET_SUCCESS_MESSAGE,
            payload: message,
        };
    },

    resetSuccess: () => {
        return {
            type: uiTypes.RESET_ERROR,
        };
    },
});
