import { ITaskModel } from '../../types/TaskModel';
import { newTaskActions, todoList } from '../../lib/redux/actions';
import { useAppDispatch } from '../../lib/redux/init/store';
import { formatDate } from '../../helpers/formatDate';

export const Task:React.FC<ITaskModel> = (props) => {
    const dispatch = useAppDispatch();

    const {
        completed, title, deadline, tag, id,
    } = props;

    const handleClick = () => {
        dispatch(newTaskActions.setShowState(true));
        dispatch(todoList.setSelectedTodoId(id));
    };

    return (
        <div onClick = { handleClick } className = { completed ? 'task completed' : 'task' }>
            <span className = 'title'>{ title }</span>
            <div className = 'meta'>
                <span className = 'deadline'>{ formatDate(deadline) }</span>
                <span
                    className = 'tag'
                    style = { { backgroundColor: tag.bg, color: tag.color } }>{ tag.name }</span>
            </div>
        </div>
    );
};
