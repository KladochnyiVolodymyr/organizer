import { ITagModel } from './TagModel';

export interface ITodoModel {
    id: string,
    completed: boolean,
    title: string,
    description: string,
    deadline: string,
    tag: ITagModel
}
