// Core
import { useEffect } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import DatePicker from 'react-datepicker';
import { useSelector } from 'react-redux';

// Components
import { Input } from '../elements';
import { TagsList } from '../../TagsList';
import { CompleteBtn } from '../elements/CompleteBtn';

// Other
import { ITaskFormShape } from '../types';
import { getNewTaskState } from '../../../lib/redux/selectors';
import { useAppDispatch } from '../../../lib/redux/init/store';
import { newTaskActions, todoList, uiActions } from '../../../lib/redux/actions';
import { api } from '../../../api';
import { ITaskModel } from '../../../types/TaskModel';
import { getTodoList } from '../../../lib/redux/selectors/todoList';
import { useCurrentTask } from '../../../hooks';
import { schema } from './config';

export const TaskCard:React.FC = () => {
    const { tags } = useSelector(getNewTaskState);
    const { selectedTodoId } = useSelector(getTodoList);

    const dispatch = useAppDispatch();
    const currentTaskData = useCurrentTask();

    const defaultValues = {
        title:       '',
        description: '',
        deadline:    new Date(),
        tag:         tags[ 0 ].id,
        completed:   false,
    };

    const form = useForm<ITaskFormShape>({
        mode:     'onTouched',
        defaultValues,
        resolver: yupResolver(schema),
    });

    const handleDelete = async () => {
        try {
            await api.todos.delete(selectedTodoId);
            dispatch(todoList.deleteTodo(selectedTodoId));
            dispatch(newTaskActions.setShowState(false));
            dispatch(uiActions.successMessage('Задача успішно видалена!'));
        } catch (error:any) {
            dispatch(uiActions.setError(error.message));
        }
    };


    const onSubmit = form.handleSubmit(async (data:ITaskModel) => {
        try {
            if (selectedTodoId) {
                const updateTodo = await api.todos.update(data, selectedTodoId);
                dispatch(todoList.updateTodo(updateTodo));
                dispatch(uiActions.successMessage('Задача успішно оновлена!'));
            } else {
                const newTodo = await api.todos.create(data);
                dispatch(todoList.addTodo(newTodo));
                dispatch(uiActions.successMessage('Задача додана!'));
            }
            dispatch(newTaskActions.setShowState(false));
        } catch (error:any) {
            dispatch(uiActions.setError(error.message));
        }
    });

    const onReset = () => {
        form.reset();
    };

    useEffect(() => {
        if (currentTaskData) {
            form.setValue('title', currentTaskData.title);
            form.setValue('description', currentTaskData.description);
            form.setValue('deadline', new Date(currentTaskData.deadline));
            form.setValue('tag', currentTaskData.tag.id);
            form.setValue('completed', currentTaskData.completed);
        }
        if (!selectedTodoId) {
            form.reset();
        }
    }, [currentTaskData, selectedTodoId]);

    return (
        <div className = 'task-card'>
            <form onSubmit = { onSubmit }>
                <div className = 'head'>
                    { selectedTodoId && (
                        <>
                            <Controller
                                control = { form.control }
                                name = 'completed'
                                render = { ({
                                    field: {
                                        onChange, value,
                                    },
                                }) => (
                                    <CompleteBtn
                                        value = { value }
                                        onChange = { onChange } />
                                ) } />
                            <div className = 'button-remove-task' onClick = { handleDelete }></div>
                        </>
                    ) }
                </div>
                <div className = 'content'>
                    <Input
                        className = 'title'
                        label = 'Задача'
                        placeholder = 'Виконати персональний проект'
                        error = { form.formState.errors.title }
                        register = { form.register('title') } />
                    <div className = 'deadline'>
                        <span className = 'label'>Дедлайн</span>
                        <span className = 'date'>
                            <Controller
                                control = { form.control }
                                name = 'deadline'
                                render = { ({
                                    field: {
                                        onChange, onBlur, value, name, ref,
                                    },
                                }) => (
                                    <DatePicker
                                        name = { name }
                                        selected = { value }
                                        minDate = { new Date() }
                                        onChange = { onChange }
                                        onBlur = { onBlur }
                                        ref = { ref } />
                                ) } />

                        </span>
                    </div>
                    <div className = 'description'>
                        <Input
                            className = 'title'
                            label = 'Опис'
                            type = 'textarea'
                            placeholder = 'Приклад опису'
                            error = { form.formState.errors.description }
                            register = { form.register('description') } />
                    </div>
                    <div className = 'tags'>
                        <Controller
                            control = { form.control }
                            name = 'tag'
                            render = { ({
                                field: {
                                    onChange, value,
                                },
                            }) => (
                                <TagsList
                                    value = { value }
                                    onChange = { onChange }
                                    tags = { tags } />
                            ) } />
                    </div>
                    <div className = 'form-controls'>
                        <button
                            onClick = { onReset } type = 'reset'
                            className = 'button-reset-task' disabled = { !form.formState.isDirty }>Reset</button>
                        <button
                            type = 'submit' className = 'button-save-task'
                            disabled = { !form.formState.isDirty }>Save</button>
                    </div>
                </div>
            </form>
        </div>
    );
};
