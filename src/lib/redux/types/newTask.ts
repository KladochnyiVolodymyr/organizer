export const newTaskTypes = Object.freeze({
    INIT_TAGS:      'INIT_TAGS',
    SET_SHOW_STATE: 'SET_SHOW_STATE',
});
