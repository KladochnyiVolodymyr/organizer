// Core
import axios from 'axios';
import { ISignUp } from '../components/forms/SignUp/config';
import {
    ILogin, ISignUpWithToken, ITagModel, ITodoModel,
} from '../types';
import { ITaskModel } from '../types/TaskModel';

export const TODO_API_URL = 'https://lab.lectrum.io/rtx/api/v2/todos';

export const api = Object.freeze({
    get token() {
        return localStorage.getItem('token');
    },
    auth: {
        async signup(userInfo: ISignUp): Promise<ISignUpWithToken> {
            const { data } = await axios.post<ISignUpWithToken>(
                `${TODO_API_URL}/auth/registration`,
                userInfo,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    },
                },
            );

            return data;
        },
        async login(credentials: string): Promise<ILogin> {
            const { data } = await axios.get<ILogin>(`${TODO_API_URL}/auth/login`,
                {
                    headers: {
                        Authorization: `Basic ${credentials}`,
                    },
                });

            return data;
        },
        async logout() {
            const { data } = await axios.get(`${TODO_API_URL}/auth/logout`,
                {
                    headers: {
                        Authorization: `Bearer ${api.token}`,
                    },
                });

            return data;
        },
    },
    todos: {
        async fetch(): Promise<ITodoModel[]> {
            const { data } = await axios.get<ITodoModel[]>(`${TODO_API_URL}/tasks`, {
                headers: {
                    Authorization: `Bearer ${api.token}`,
                },
            });

            return data;
        },
        async create(task: ITaskModel): Promise<ITaskModel> {
            const { data } = await axios.post<ITaskModel>(
                `${TODO_API_URL}/tasks`,
                task,
                {
                    headers: {
                        Authorization: `Bearer ${api.token}`,
                    },
                },
            );

            return data;
        },
        async update(task:ITaskModel, id: string): Promise<ITodoModel> {
            const { data } = await axios.put<ITodoModel>(
                `${TODO_API_URL}/tasks/${id}`,
                task,
                {
                    headers: {
                        Authorization: `Bearer ${api.token}`,
                        id:            `${id}`,
                    },
                },
            );

            return data;
        },
        async delete(id: string) {
            const { data } = await axios.delete(
                `${TODO_API_URL}/tasks/${id}`,
                {
                    headers: {
                        Authorization: `Bearer ${api.token}`,
                        id:            `${id}`,
                    },
                },
            );

            return data;
        },
    },
    tags: {
        async getTags(): Promise<ITagModel[]> {
            const { data } = await axios.get<ITagModel[]>(`${TODO_API_URL}/tags`);

            return data;
        },
    },
});
