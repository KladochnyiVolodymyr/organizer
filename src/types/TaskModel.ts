import { ITagModel } from './TagModel';

export interface ITaskModel {
    id: string;
    completed: boolean;
    title: string;
    deadline: string;
    tag: ITagModel;
}
