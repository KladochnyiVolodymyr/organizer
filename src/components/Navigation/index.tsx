// Core
import { useDispatch } from 'react-redux';
import { NavLink, useNavigate } from 'react-router-dom';
import { api } from '../../api';
import { uiActions } from '../../lib/redux/actions';
import { authActions } from '../../lib/redux/actions/auth';


export const Navigation:React.FC = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const { token } = api;

    const handleLogout = async () => {
        try {
            await api.auth.logout();
            dispatch(authActions.setToken(''));
            localStorage.removeItem('token');
            navigate('/login');
            dispatch(uiActions.successMessage('До зустрічі!'));
        } catch (error: any) {
            dispatch(uiActions.setError(error.message));
        }
    };

    return (
        <nav>
            { !token && (
                <NavLink
                    to = '/login'
                    activeClassName = 'active'>Ввійти</NavLink>
            ) }

            <NavLink
                to = '/task-manager'
                className = { !token ? 'is-disabled' : '' }
                activeClassName = 'active'>До задач</NavLink>
            <NavLink
                to = '/profile'
                className = { !token ? 'is-disabled' : '' }
                activeClassName = 'active'>Профіль</NavLink>
            { token && (
                <button className = 'button-logout' onClick = { handleLogout }>Вийти</button>
            ) }

        </nav>
    );
};
