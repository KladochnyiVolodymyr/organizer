// Core
import { Link, useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useDispatch } from 'react-redux';

// Components
import { Input } from '../elements';

// Other
import { schema } from './config';
import { ISignUpFormShape } from '../types';
import { authActions } from '../../../lib/redux/actions/auth';

// Hooks
import { api } from '../../../api';
import { uiActions } from '../../../lib/redux/actions';

export const SignUpForm: React.FC = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const form = useForm<ISignUpFormShape>({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });


    const onSubmit = form.handleSubmit(async (data: ISignUpFormShape) => {
        const { confirmPassword, ...newUser } = data;
        try {
            const result = await api.auth.signup(newUser);
            const token = result.data;

            dispatch(authActions.setToken(token));
            localStorage.setItem('token', token);
            navigate('/task-manager');
            form.reset();
            dispatch(uiActions.successMessage('Ласкаво просимо!'));
        } catch (error:any) {
            dispatch(uiActions.setError(error.message));
        }
    });

    return (
        <form onSubmit = { onSubmit }>
            <fieldset>
                <legend>Реєстрація</legend>
                <Input
                    placeholder = "Ім'я"
                    error = { form.formState.errors.name }
                    register = { form.register('name') } />
                <Input
                    placeholder = 'Пошта'
                    error = { form.formState.errors.email }
                    register = { form.register('email') } />
                <Input
                    placeholder = 'Пароль'
                    type = 'password'
                    error = { form.formState.errors.password }
                    register = { form.register('password') } />

                <Input
                    placeholder = 'Підтвердження пароля'
                    type = 'password'
                    error = { form.formState.errors.confirmPassword }
                    register = { form.register('confirmPassword') } />

                <input
                    className = 'button-login' type = 'submit'
                    value = 'Зареєструватись' />
            </fieldset>
            <p>
                Перейти до <Link to = '/login'>логіну</Link>
            </p>
        </form>
    );
};
