import { AnyAction } from 'redux';
import { ITaskStateModel } from '../../../types/ITaskStateModel';
import { newTaskTypes } from '../types';

export const initialState:ITaskStateModel = {
    tags:          [],
    isOpenNewTask: false,
};

// eslint-disable-next-line default-param-last
export const newTaskReducer = (state = initialState, action:AnyAction) => {
    switch (action.type) {
        case newTaskTypes.INIT_TAGS: {
            return {
                ...state,
                tags: action.payload,
            };
        }

        case newTaskTypes.SET_SHOW_STATE: {
            return {
                ...state,
                isOpenNewTask: action.payload,
            };
        }

        default: {
            return state;
        }
    }
};
