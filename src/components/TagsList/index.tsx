import { ITagModel } from '../../types';
import { Tag } from '../Tag';

interface IProps {
    tags: ITagModel[],
    value: string,
    onChange: (id: string) => void
}

export const TagsList = (props: IProps) => {
    const { tags, value, onChange } = props;

    return (
        <>
            {
                tags.map(
                    (tag) => <Tag
                        key = { tag.id }
                        { ...tag }
                        onSelect = { onChange }
                        isSelected = { value === tag.id } />,
                )
            }
        </>
    );
};
