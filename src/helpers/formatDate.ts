export const formatDate = (date:string) => {
    if (!date) {
        return null;
    }

    const formattedDate = new Date(date).toLocaleDateString('en-US');

    return formattedDate;
};
