import { api } from '../../../api';
import { ITagModel } from '../../../types';
import { AppThunk } from '../init/store';
import { newTaskTypes } from '../types/newTask';
import { uiActions } from './ui';


export const newTaskActions = Object.freeze({

    initTags: (tags: ITagModel[]) => {
        return {
            type:    newTaskTypes.INIT_TAGS,
            payload: tags,
        };
    },

    setShowState: (isOpenNewTask: boolean) => {
        return {
            type:    newTaskTypes.SET_SHOW_STATE,
            payload: isOpenNewTask,
        };
    },

    fetchTagsAsync: (): AppThunk => async (dispatch) => {
        try {
            const tags = await api.tags.getTags();

            dispatch(newTaskActions.initTags(tags));
        } catch (error:any) {
            dispatch(uiActions.setError(error.message));
        }
    },
});
