import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { toastOptions } from '../constans/toastOptions';
import { uiActions } from '../lib/redux/actions';
import { getErrorMessage, getSuccessMessage } from '../lib/redux/selectors';

export const useErrorMessage = () => {
    const errorMessage = useSelector(getErrorMessage);
    const successMessage = useSelector(getSuccessMessage);
    const dispatch = useDispatch();

    useEffect(() => {
        if (errorMessage) {
            toast.error(errorMessage, toastOptions);

            dispatch(uiActions.resetError());
        }
        if (successMessage) {
            toast.success(successMessage, toastOptions);
            dispatch(uiActions.resetSuccess());
        }
    }, [errorMessage, successMessage]);
};

