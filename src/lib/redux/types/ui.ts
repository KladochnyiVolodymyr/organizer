export const uiTypes = Object.freeze({
    SET_ERROR:           'SET_ERROR',
    RESET_ERROR:         'RESET_ERROR',
    RESET_SUCCESS:       'RESET_SUCCESS',
    SET_SUCCESS_MESSAGE: 'SET_SUCCESS_MESSAGE',
});
