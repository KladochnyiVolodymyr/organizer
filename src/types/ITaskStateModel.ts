import { ITagModel } from './TagModel';

export interface ITaskStateModel {
    tags: ITagModel[];
    isOpenNewTask: boolean
}
