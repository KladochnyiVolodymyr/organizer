export interface ITagModel {
    id: string;
    bg: string;
    color: string;
    name: string
}
