// Core
import { combineReducers } from 'redux';

// Reducers
import {
    newTaskReducer as newTask,
    authReducer as auth,
    todoListReducer as todoList,
    uiReducer as ui,
} from '../reducers';

export const rootReducer = combineReducers({
    newTask,
    auth,
    todoList,
    ui,
});
