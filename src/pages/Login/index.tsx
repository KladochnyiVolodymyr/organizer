// Components
import { LoginForm } from '../../components/forms/Login';

export const Login: React.FC = () => {
    return (
        <section className = 'sign-form'>
            <LoginForm />
        </section>
    );
};
