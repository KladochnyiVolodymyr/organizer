import { AnyAction } from 'redux';
import { uiTypes } from '../types';

const initialState = {
    successMessage: '',
    errorMessage:   '',
    error:          false,
};

// eslint-disable-next-line default-param-last
export const uiReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case uiTypes.RESET_ERROR: {
            return {
                ...state,
                error:        false,
                errorMessage: '',
            };
        }

        case uiTypes.SET_ERROR: {
            return {
                ...state,
                error:        true,
                errorMessage: action.payload,
            };
        }

        case uiTypes.SET_SUCCESS_MESSAGE: {
            return {
                ...state,
                successMessage: action.payload,
            };
        }

        case uiTypes.RESET_SUCCESS: {
            return {
                ...state,
                successMessage: '',
            };
        }

        default: {
            return state;
        }
    }
};
