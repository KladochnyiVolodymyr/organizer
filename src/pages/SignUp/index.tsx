// Components
import { SignUpForm } from '../../components/forms/SignUp';

export const SignUp: React.FC = () => {
    return (
        <section className = 'publish-tip sign-form'>
            <SignUpForm />
        </section>
    );
};
