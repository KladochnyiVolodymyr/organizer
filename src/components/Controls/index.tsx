import { newTaskActions, todoList } from '../../lib/redux/actions';
import { useAppDispatch } from '../../lib/redux/init/store';

export const Controls:React.FC = () => {
    const dispatch = useAppDispatch();

    const handleClick = () => {
        dispatch(newTaskActions.setShowState(true));
        dispatch(todoList.setSelectedTodoId(''));
    };

    return (
        <div className = 'controls'>
            <i className = 'las'></i>
            <button onClick = { handleClick } className = 'button-create-task'>Нова задача</button>
        </div>
    );
};
