// Core
import { Link, useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

// Components
import { useDispatch } from 'react-redux';
import { Input } from '../elements';

// Other
import { ILoginFormShape } from '../types';
import { schema } from './config';
import { api } from '../../../api';
import { authActions } from '../../../lib/redux/actions/auth';
import { uiActions } from '../../../lib/redux/actions';

export const LoginForm: React.FC = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const form = useForm<ILoginFormShape>({
        mode:     'onTouched',
        resolver: yupResolver(schema),
    });

    const onSubmit = form.handleSubmit(async (credentials: ILoginFormShape) => {
        const baseFormat = window.btoa(`${credentials.email}:${credentials.password}`);
        try {
            const result = await api.auth.login(baseFormat);
            const token = result.data;

            dispatch(authActions.setToken(token));
            localStorage.setItem('token', token);
            navigate('/task-manager');
            form.reset();
            dispatch(uiActions.successMessage('Ласкаво просимо!'));
        } catch (error: any) {
            dispatch(uiActions.setError(error.message));
        }
    });

    return (
        <form onSubmit = { onSubmit }>
            <fieldset>
                <legend>Вхід</legend>
                <Input
                    placeholder = 'Пошта'
                    error = { form.formState.errors.email }
                    register = { form.register('email') } />
                <Input
                    placeholder = 'Пароль'
                    type = 'password'
                    error = { form.formState.errors.password }
                    register = { form.register('password') } />
                <input
                    className = 'button-login' type = 'submit'
                    value = 'Ввійти' />

            </fieldset>
            <p>
                Якщо у вас ще немає облікового запису, ви можете <Link to = '/signup'>зареєструватись</Link>
            </p>
        </form>
    );
};
