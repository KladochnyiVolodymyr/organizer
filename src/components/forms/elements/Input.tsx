import { UseFormRegisterReturn } from 'react-hook-form';

export const Input: React.FC<IPropTypes> = (props) => {
    const input = (
        <input
            className = { props.className }
            placeholder = { props.placeholder }
            type = { props.type }
            { ...props.register } />
    );

    const textarea = (
        <textarea
            className = { props.className }
            placeholder = { props.placeholder }
            { ...props.register }>
        </textarea>
    );

    return (
        <label className = 'label'>
            { props.label } { ' ' }
            <span className = 'errorMessage'>{ props.error?.message }</span>
            { props.type === 'textarea' ? textarea : input }
        </label>
    );
};

Input.defaultProps = {
    type: 'text',
    tag:  'input',
};

interface IPropTypes {
    placeholder?: string;
    className?: string
    type?: string;
    tag?: string;
    label?: string;
    register: UseFormRegisterReturn;
    error?: {
        message?: string;
    };
    options?: { value: string; name: string }[];
}

