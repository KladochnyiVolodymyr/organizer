// Core
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { todoList } from '../../lib/redux/actions';
import { useAppDispatch } from '../../lib/redux/init/store';
import { getTodoList } from '../../lib/redux/selectors/todoList';
import { ITaskModel } from '../../types/TaskModel';
import { Task } from '../Task';

export const List:React.FC = () => {
    const dispatch = useAppDispatch();
    const { todos } = useSelector(getTodoList);

    useEffect(() => {
        dispatch(todoList.fetchTodoListAsync());
    }, []);

    const tasksJSX = todos.map((task:ITaskModel) => <Task key = { task.id } { ...task } />);

    return (
        <div className = { todos.length > 0 ? 'list' : 'list empty' }>
            <div className = 'tasks'>
                { tasksJSX }
            </div>
        </div>
    );
};
